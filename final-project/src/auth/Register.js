import React, { useContext, useState } from "react"
import axios from "axios"
import Cookies from "js-cookie"
import { useHistory } from "react-router"
import './Auth.css'

const Register = () => {
    let history = useHistory()
    const [input, setInput] = useState({ name: "", email: "", password: "" })

    const handleSubmit = (event) => {
        event.preventDefault()
        axios.post("https://backendexample.sanbersy.com/api/register", {
            name: input.name,
            email: input.email,
            password: input.password
        }).then(
            () => {
                history.push('/login')
            }
        ).catch((err) => {
            alert(err)
        })
    }

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name

        setInput({ ...input, [name]: value })
    }

    return (
        <div className="auth content" >
          <form onSubmit={handleSubmit}>
            <label>name: </label>
            <br />
            <input type="text" name="name" onChange={handleChange} value={input.name} />
            <br />
            <label>email: </label>
            <br />
            <input type="email" name="email" onChange={handleChange} value={input.email} />
            <br />
            <label>Password: </label>
            <br />
            <input type="password" name="password" onChange={handleChange} value={input.password} />
            <br />
            <br />
            <button>Register</button>
          </form>
        </div>
    )
}

export default Register