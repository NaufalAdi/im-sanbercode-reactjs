import React, { useContext, useState } from "react"
import axios from "axios"
import Cookies from "js-cookie"
import { useHistory } from "react-router"
import './Auth.css'

const Register = () => {
    let history = useHistory()
    const [input, setInput] = useState(
      { current_password: "", 
        new_password: "", 
        new_confirm_password: "" 
      })

    const handleSubmit = (event) => {
        event.preventDefault()
        axios.post("https://backendexample.sanbersy.com/api/change-password", 
          input,
          {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}}
          
        ).then(
            () => {
                history.push('/login')
            }
        ).catch((err) => {
            alert("Invalid credentials")
            console.log(err)
        })
    }

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name

        setInput({ ...input, [name]: value })
    }

    return (
        <div className="auth content">
          <form onSubmit={handleSubmit}>
            <label>Current Password: </label>
            <br />
            <input type="password" name="current_password" onChange={handleChange} value={input.current_password} />
            <br />
            <label>New Pasword: </label>
            <br />
            <input type="password" name="new_password" onChange={handleChange} value={input.new_password} />
            <br />
            <label>Confirm New Password: </label>
            <br />
            <input type="password" name="new_confirm_password" onChange={handleChange} value={input.new_confirm_password} />
            <br />
            <button>Register</button>
          </form>
        </div>
    )
}

export default Register