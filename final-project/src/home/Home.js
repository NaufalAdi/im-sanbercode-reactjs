import React, { useContext, useEffect } from 'react'
import { useHistory } from 'react-router'
import { Card, Button, Typography } from 'antd'
import { GameContext } from '../game/GameContext'
import './Home.css'
import { MovieContext } from '../movie/MovieContext'
import Text from 'antd/lib/typography/Text'
import { Content } from 'antd/lib/layout/layout'

const Home = () => {
  const { movies, fetchMovies } = useContext(MovieContext)
  const { games, fetchGames } = useContext(GameContext)

  useEffect(()=>{
    fetchGames()
    fetchMovies()
  },[])

  return (
    <Content>
      <div className="content-home content" id="home-movies"> 
        <div className="section-header">
          <Typography.Title level={2}>Movies</Typography.Title>        
          <Button className="page-nav-btn" type="link" size="large">
          <a href="#home-games">Go to Games</a>
          </Button>
        </div>
        {movies.map(item => {
          return (
            <ItemCard item={item} url={`/movie-details/${item.id}`} />
          )}
        )}
      </div>
      <div className="content-home content" id="home-games">
        <div className="section-header">
          <Typography.Title level={2}>Games</Typography.Title>  
          <Button className="page-nav-btn" type="link" size="large">
            <a href="#home-movies">Go to Movies</a>
          </Button>
        </div>
        {games.map(item => {
          return (
            <ItemCard item={item} url={`/game-details/${item.id}`} />
          )}
        )}
      </div>
    </Content>
  )
}

const ItemCard = (props) => {
  const { Meta } = Card
  let history = useHistory()

  const handleClick = () => {
    history.push(props.url)
  }

  const renderTitle = () => {
    let title = props.item.name !== undefined? props.item.name : props.item.title
    return (
      <Text ellipsis>{title}
        <Text type="secondary"> ({props.item.year !== undefined? props.item.year : props.item.release}) </Text>
      </Text>
    )
  }

  return (
    <div className="item-card-container">
      <Card className="item-card"
      hoverable
      key={props.item.id.toString()}
      onClick={handleClick}
      cover={<img alt="example" src={props.item.image_url} />}
      >
        <Meta 
          title={renderTitle()} 
          description={<Text ellipsis>{props.item.genre}</Text>} 
        />
      </Card>
    </div>
  )
}


export default Home
