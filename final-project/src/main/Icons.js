import {ReactComponent as MovieSvg} from './ic_movie_black_24dp.svg'
import {ReactComponent as GameSvg} from './ic_sports_esports_black_24dp.svg'
import {ReactComponent as HomeSvg} from './ic_home_black_24dp.svg'
import {ReactComponent as AppIconSvg} from './ic_live_tv_white_24dp.svg'
import {ReactComponent as ProfileSvg} from './ic_account_circle_black_24dp.svg'
import Icon from '@ant-design/icons';

export const HomeOutlined = props => <Icon component={HomeSvg} {...props} />;
export const MovieOutlined = props => <Icon component={MovieSvg} {...props} />;
export const GameOutlined = props => <Icon component={GameSvg} {...props} />;
export const AppIcon = props => <Icon component={AppIconSvg} {...props} />;
export const AccountOutlined = props => <Icon component={ProfileSvg} {...props} />
