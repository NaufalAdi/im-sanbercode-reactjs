import Cookies from "js-cookie";
import React, { useContext, useState } from "react"
import { Link } from "react-router-dom";
import { UserContext } from "../auth/UserContext";
import { useHistory } from "react-router";
import { Menu, Layout, Button } from 'antd'
import { Header } from "antd/lib/layout/layout";
import { HomeOutlined, MovieOutlined, GameOutlined, AppIcon, AccountOutlined } from './Icons'
import logo from './logo.png'


export const NavSider = () => {
  const { Sider } = Layout
  const [ collapsed, setCollapsed ] = useState(false)

  const onCollapse = collapsed => setCollapsed(collapsed)

  return (
    <>
      {Cookies.get('token') !== undefined &&
        <Sider width={200} 
          theme='light'
          collapsible 
          collapsed={collapsed} 
          onCollapse={onCollapse}
          style={{
            overflow: 'visible',
            height: '140%',
            position: 'fixed',
            top: 0,
            left: 0,
            zIndex: 29,
            boxShadow: "1px 1px 10px hsl(0, 0%, 70%)"
          }}
        >
          <Menu
            theme='light'
            mode='inline'
            defaultSelectedKeys={['1']}
            style={{
              height: "140%",
              paddingTop: 60,
            }}
          >
            <Menu.Item key="1" icon={<HomeOutlined style={{fontSize: "18px"}}/>}><Link to="/">Home</Link></Menu.Item>
            <Menu.SubMenu key="sub1" title="Movies" icon={<MovieOutlined style={{fontSize: "18px"}}/>}>
              <Menu.Item key="2"><Link to="/movie">Movie List</Link></Menu.Item>
              <Menu.Item key="3"><Link to="/movie/create">Add a Movie</Link></Menu.Item>
            </Menu.SubMenu>
            <Menu.SubMenu key="sub2" title="Games" icon={<GameOutlined style={{fontSize: "18px"}}/>}>
              <Menu.Item key="4"><Link to="/game">Game List</Link></Menu.Item>
              <Menu.Item key="5"><Link to="/game/create">Add a Game</Link></Menu.Item>
            </Menu.SubMenu>
          </Menu>
        </Sider>
      }
    </>
  )
}

export const NavHeader = () => {
  const { setLoginStatus } = useContext(UserContext)
  const [ collapsed, setCollapsed ] = useState(true)
  let history = useHistory()

  const handleLogout = () => {
    setCollapsed(true)
    setLoginStatus(false)
    Cookies.remove('user')
    Cookies.remove('email')
    Cookies.remove('token')
    history.push('/login')
  }

  return (
    <Header id="header">
      <div className="logo-button" onClick={()=>history.push("/")}>
        <AppIcon style={{color: "#FFFFFF", fontSize: '24px'}}/>
        <span style={{color: "#FFFFFF", fontSize: '20px'}}> GMovies </span>
      </div>
      <Button 
        ghost
        type='link'
        shape="circle" 
        icon={<AccountOutlined style={{color: "#FFFFFF", fontSize: '30px'}} onClick={() => setCollapsed(!collapsed)}/> }
        style={{float: "right", top: "13px"}}
        />
      <Menu 
        id="navbar-menu" 
        theme='dark'
        mode='vertical'
        style={collapsed ? {display: "none"} : {display: "block"}}
      >
      {
        Cookies.get('token') !== undefined &&
        <>
          <Menu.Item key="2" onClick={() => setCollapsed(true)}>
            <Link to="/change-password">Change Password</Link>
          </ Menu.Item>
          <Menu.Item key="1" onClick={handleLogout}>
            Logout
          </ Menu.Item>
        </>
      }
      {Cookies.get('token') === undefined &&
      <>
        <Menu.Item key="3" onClick={() => setCollapsed(true)}>
          <Link to="/login">Login</Link>
        </Menu.Item>
        <Menu.Item key="4" onClick={() => setCollapsed(true)}>
          <Link to="/register">Register</Link>
        </Menu.Item>
      </>
      }
      </Menu>
    </Header>
  )
}
