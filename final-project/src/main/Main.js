import React, { useContext, useEffect } from "react"
import {BrowserRouter as Router, Switch, Route, Redirect} from "react-router-dom";
import Login from "../auth/Login"
import Cookies from "js-cookie";
import { UserContext  } from "../auth/UserContext";
import { Layout } from 'antd';
import Register from "../auth/Register";
import ChangePassword from "../auth/ChangePassword";
import Home from "../home/Home";
import MovieDetails from "../details/MovieDetails";
import GameDetails from "../details/GameDetails";
import { NavSider, NavHeader } from "./Navbar";
import MovieList from "../movie/MovieList";
import MovieCreateForm from "../movie/MovieCreateForm";
import MovieEditForm from "../movie/MovieEditForm";
import GameList from "../game/GameList";
import GameCreateForm from "../game/GameCreateForm";
import GameEditForm from "../game/GameEditForm";
import { GameProvider } from "../game/GameContext";
import { MovieProvider } from "../movie/MovieContext";

const Main = () => {
  const { Footer } = Layout;

  const { loginStatus, setLoginStatus } = useContext(UserContext)

  useEffect(() => {
    Cookies.get('token')  !== undefined ? setLoginStatus(true) : setLoginStatus(false)
  })
  //Sebelum Login
  const LoginRoute = ({ ...props }) =>
    Cookies.get('token') !== undefined ? <Redirect to="/" /> : <Route {...props} />;

  //Setelah Login
  const PrivateRoute =({...props }) =>
    Cookies.get('token') === undefined ? <Redirect to="/" /> : <Route {...props} />;

  return (
    <GameProvider><MovieProvider>
      <Layout>
        <Router>
          <NavHeader/>
          <Layout id="main" style={loginStatus ? {marginLeft: "80px"}:{}}>
            <NavSider />
              <Switch>
                <Route path="/"  exact  component={Home} />
                <Route path="/movie-details/:id" component={MovieDetails} />
                <Route path="/game-details/:id" component={GameDetails} />
                <PrivateRoute path="/change-password" component={ChangePassword} />
                <PrivateRoute exact path="/movie" component={MovieList} />
                <PrivateRoute path="/movie/create" component={MovieCreateForm} />
                <PrivateRoute path="/movie/edit/:id" component={MovieEditForm} />
                <PrivateRoute exact path="/game" component={GameList} />
                <PrivateRoute path="/game/create" component={GameCreateForm} />
                <PrivateRoute path="/game/edit/:id" component={GameEditForm} />
                <LoginRoute exact path="/login" component={Login} />
                <LoginRoute exact path="/register" component={Register} />
              </Switch>
          </Layout>
          <Footer id="footer">
            <h5>&copy; Naufal Adi 2021</h5>
          </Footer>
        </Router>
      </Layout>
    </MovieProvider></GameProvider>
  )
}

export default Main