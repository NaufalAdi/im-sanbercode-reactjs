import { message } from 'antd';
import axios from 'axios';
import React, { createContext, useState } from 'react'
import { useHistory } from 'react-router';
export const MovieContext = createContext();
export const MovieProvider = props => {
  const [ movies, setMovies] = useState([])
  let history = useHistory()

  const fetchMovies = async () => {
    console.log("Fetching Movies")
    axios.get("https://backendexample.sanbersy.com/api/data-movie")
      .then( res => {
        setMovies(res.data)
      }).catch((err) => {
        message.error(err.message)
    })
  }

  const fetchMovieById = async (id) => {
    try {
      let res = await axios.get(`https://backendexample.sanbersy.com/api/data-movie/${id}`)
      return res.data
    } catch(err) {
      message.error(err.message)
      history.goBack()
    }
  }

  return (
    <MovieContext.Provider value={{
      movies, 
      setMovies,
      fetchMovies,
      fetchMovieById
      }}
    >
      {props.children}
    </MovieContext.Provider>
  );
};
