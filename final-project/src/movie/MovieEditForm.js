import React, {useContext, useEffect, useState} from 'react'
import { Form, Input, Button, InputNumber } from 'antd';
import './MovieForm.css'
import axios from 'axios';
import Cookies from 'js-cookie';
import { useHistory, useParams } from 'react-router-dom';
import { Content } from 'antd/lib/layout/layout';
import { MovieContext } from './MovieContext';

const MovieEditForm = () => {
  const { fetchMovieById } = useContext(MovieContext)
  const [ isLoaded, setIsLoaded ] = useState(false)
  const [form] = Form.useForm()
  let history = useHistory()
  let { id } = useParams()

  const init = async () => {
    try {
      let data = await fetchMovieById(id)
      form.setFieldsValue(data)
      setIsLoaded(true)
    } catch {
      history.push('/movie')
    }
  }

  const handleSubmit = () => {
    var data = form.getFieldsValue()
    console.log(data)
    
    axios.put(`https://backendexample.sanbersy.com/api/data-movie/${id}`, 
      data,
      {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}}
    ).then(
        () => {
            history.push('/movie')
        }
    ).catch((err) => {
        alert("Invalid credentials")
        console.log(err)
    })
  }

  useEffect(() => {
    init()
  }, [])

  return (
    <Content className="content-home content">
      <h1> Edit Movie </h1>
      <Form
        form={form}
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={handleSubmit}
        autoComplete="off"
      >
        <Form.Item
          label="Title"
          name="title"
          rules={[{ required: true }]}
        >
          <Input/>
        </Form.Item>

        <Form.Item
          label="Description"
          name="description"
          rules={[{ required: true }]}
        >
          <Input.TextArea/>
        </Form.Item>

        <Form.Item
          label="Duration"
          name="duration"
          rules={[{ required: true }]}
        >
          <InputNumber min={0}/>
        </Form.Item>

        <Form.Item
          label="Genre"
          name="genre"
          rules={[{ required: true }]}
        >
          <Input/>
        </Form.Item>

        <Form.Item
          label="Image URL"
          name="image_url"
          rules={[{ required: true }]}
        >
          <Input/>
        </Form.Item>

        <Form.Item
          label="Rating"
          name="rating"
          rules={[{ required: true }]}
        >
          <InputNumber min={0} max={10}/>
        </Form.Item>

        <Form.Item
          label="Review"
          name="review"
          rules={[{ required: true }]}
        >
          <Input.TextArea/>
        </Form.Item>

        <Form.Item
          label="Year"
          name="year"
          rules={[{ required: true }]}
        >
          <InputNumber min={1980} max={2021}/>
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit" disabled={!isLoaded}>
            Submit
          </Button>
        </Form.Item>
        
      </Form>
      </Content>
  )
}

export default MovieEditForm
