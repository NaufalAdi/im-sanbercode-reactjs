import React, {useContext, useEffect, useState} from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'
import { message, Table, Button, Space, Form, Input, InputNumber, Typography, Divider } from 'antd'
import { DeleteFilled, EditOutlined } from '@ant-design/icons'
import './MovieList.css'
import Cookies from 'js-cookie'
import { Content } from 'antd/lib/layout/layout'
import { MovieContext } from './MovieContext'

const MovieList = () => {
  const { movies, setMovies, fetchMovies } = useContext(MovieContext)
  const [ isSearching, setIsSearching ] = useState(false)
  const [ filteredData, setFilteredData ] = useState([])
  const [ searchedData, setSearchedData ] = useState([])
  const [ searchInput, setSearchInput ] = useState("")
  const [ filterForm ] = Form.useForm()
  const { Paragraph } = Typography

  const handleDelete = (event) => {
    let id = event.currentTarget.value
    axios.delete(`https://backendexample.sanbersy.com/api/data-movie/${id}`,
    {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}}
  ).then(async () => {
      let newMovies = movies.filter(el => {
        return el.id !== parseInt(id)
      })
      setMovies(newMovies)
      setFilteredData(newMovies)
      message.success("Item deleted successfully")
  }).catch((err) => {
      message.error("An error has occured")
      console.log(err)
  })
}

  const clearForm = () => {
    filterForm.resetFields()
    filterForm.submit()
  }

  const onFilter = () => {
    let query = filterForm.getFieldsValue()
    setIsSearching(false)
    setSearchInput("")
    let filteredData = movies
    filteredData = movies.filter(el => (
        (query.genre !== '' ? 
          el.genre.toLowerCase().includes(query.genre.toLowerCase()) : true
        ) && (query.rating !== null ? 
          el.rating === query.rating : true 
        ) && (query.year !== null ? 
          el.year === query.year : true 
        )
    ))
    setFilteredData(filteredData)
  }

  const onTitleSearch = event => {
    let query = event.currentTarget.value
    setSearchInput(query)
    setIsSearching(true)
    setSearchedData(filteredData.filter(
      el => el.title.toLowerCase().includes(query.toLowerCase())
    ))
  }

  useEffect(() => {
    fetchMovies()
  }, [])

  useEffect(() => {
    setFilteredData(movies)
  }, [movies])

  const columns = [
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title',
      width: 120,
      sorter: {
        compare: (a, b) => a.title.localeCompare(b.title),
        multiple: 1
      }
    },
    {
      title: 'Cover',
      dataIndex: 'image_url',
      key: 'image_url',
      width: 160,
      render: image_url => (
        <img style={{width:"150px", maxHeight:"180px", objectFit: "cover"}} src={image_url} alt=""/>
      )
    },
    {
      title: 'Genre',
      dataIndex: 'genre',
      key: 'genre',
      width: 100,
      sorter: {
        compare: (a, b) => a.genre.localeCompare(b.genre),
        multiple: 1
      }
    },
    {
      title: 'Rating',
      dataIndex: 'rating',
      key: 'rating',
      width: 90,
      sorter: {
        compare: (a, b) => a.rating - b.rating,
        multiple: 1
      }
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
      width: 300,
      sorter: {
        compare: (a, b) => a.description.localeCompare(b.description),
        multiple: 1
      },
      render: description => (
        <Paragraph ellipsis={true?{ rows: 5, tooltip: description}: false}>
          {description}
        </Paragraph>
      )
    },
    {
      title: 'Duration',
      dataIndex: 'duration',
      key: 'duration',
      width: 100,
      sorter: {
        compare: (a, b) => a.duration - b.duration,
        multiple: 1
      }
    },
    {
      title: 'Review',
      dataIndex: 'review',
      key: 'review',
      width: 200,
      sorter: {
          compare: (a, b) => a.description.localeCompare(b.description),
          multiple: 1
        },
        render: review => (
          <Paragraph ellipsis={true?{ rows: 5, tooltip: review}: false}>
          {review}
        </Paragraph>
        )
    },
    {
      title: 'Year',
      dataIndex: 'year',
      key: 'year',
      width: 100,
      sorter: {
        compare: (a, b) => a.year - b.year,
        multiple: 1
      }
    },
    {
      title: 'Action',
      key: 'action',
      width: 140,
      render: (item) => (
        <Space size="middle">
            <Link to={`/movie/edit/${item.id}`}>
                <Button type="warning" ><EditOutlined /></Button>
            </Link>
            <Button danger type="primary" onClick={handleDelete} value={item.id} ><DeleteFilled/></Button>
        </Space>
      ),
    },
  ];


  return (
    <Content>
      <div className="filter-container content">
        <Divider style={{marginTop: 0}} orientation="left">Filter</Divider>
        <Form
          form={filterForm}
          name="filterForm"
          onFinish={onFilter}
          labelCol= {{ xs: { span: 4 }, sm: { span: 4 }, md: { span: 3 }, lg: { span: 2 } }}
          wrapperCol = {{ xs: { span: 18 }, sm: { span: 12 }, md: { span: 12 }, lg: { span: 12 } }}
          initialValues={{
            genre: "",
            rating: null,
            year: null
          }}
          >
          <Form.Item label="Genre"name="genre" >
            <Input />
          </Form.Item>
          
          <Form.Item label="Rating" name="rating">
            <InputNumber min={0} max={10} />
          </Form.Item>

          <Form.Item label="Year"name="year">
            <InputNumber />
              <div style={{float: "right", right: 0}}>
                <Space>
                <Button htmlType="submit">Submit</Button>
                <Button danger onClick={clearForm}>Clear</Button>
                </Space>
              </div>
          </Form.Item>
        </Form>
      </div>
      <div className="table-container content">
        <Button className="item-add-btn" type="primary">
          <Link to="/movie/create" >+ Add Movie</Link>
        </Button>
        <Input.Search 
          value={searchInput} 
          className="item-title-search" 
          style={{ maxWidth:"240px"}} 
          placeholder="Search title" 
          onChange={onTitleSearch} 
          allowClear={true}
        />
        <Table 
          columns={columns} 
          dataSource={isSearching ? searchedData: filteredData} 
          scroll={{ x: "100%"}} 
          rowKey="movie"/>
      </div>
    </Content>
  )
}

export default MovieList
