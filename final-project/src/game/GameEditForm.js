import React, {useContext, useEffect, useState} from 'react'
import { Form, Input, Button, InputNumber, Checkbox } from 'antd';
import axios from 'axios';
import Cookies from 'js-cookie';
import { useHistory, useParams } from 'react-router-dom';
import { Content } from 'antd/lib/layout/layout';
import { GameContext } from './GameContext';

const GameEditForm = () => {
  const [form] = Form.useForm()
  const [ isLoaded, setIsLoaded ] = useState(false)
  const { fetchGameById } = useContext(GameContext)
  let { id } = useParams()
  let history = useHistory()

  const init = async () => {
    try {
      let data = await fetchGameById(id)
      data.singlePlayer = !!data.singlePlayer
      data.multiplayer = !!data.multiplayer
      form.setFieldsValue(data)
      setIsLoaded(true)
    } catch (err) {
      history.push('/game')
    }
  }

  const handleSubmit = () => {
    var data = form.getFieldsValue()
    data.singlePlayer = +data.singlePlayer
    data.multiplayer = +data.multiplayer
    console.log(data)
    
    axios.put(`https://backendexample.sanbersy.com/api/data-game/${id}`, 
      data,
      {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}}
    ).then(() => {
            history.push('/game')
      }
    ).catch((err) => {
        alert("Invalid credentials")
        console.log(err)
    })
  }

  useEffect(() => {
    init()
  }, [])

  return (
    <Content className="content-home content">
      <h1> Edit Game </h1>
      <Form
        form={form}
        name="game"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={handleSubmit}
        autoComplete="off"
      >
        <Form.Item
          label="Name"
          name="name"
          rules={[{ required: true }]}
        >
          <Input/>
        </Form.Item>

        <Form.Item
          label="Image URL"
          name="image_url"
          rules={[{ required: true }]}
        >
          <Input.TextArea autoSize/>
        </Form.Item>

        <Form.Item
          label="Genre"
          name="genre"
          rules={[{ required: true }]}
        >
          <Input/>
        </Form.Item>

        <Form.Item
          label="Release"
          name="release"
          rules={[{ required: true }]}
        >
          <InputNumber min={2000} max={2021}/>
        </Form.Item>

        <Form.Item
          label="Platform"
          name="platform"
          rules={[{ required: true }]}
        >
          <Input.TextArea autoSize/>
        </Form.Item>

        <Form.Item 
          valuePropName="checked"
          name="singlePlayer" 
          wrapperCol={{ offset: 8, span: 16 }}
        >
          <Checkbox>Singleplayer</Checkbox>
        </Form.Item>

        <Form.Item 
          valuePropName="checked"
          name="multiplayer" 
          wrapperCol={{ offset: 8, span: 16 }}
        >
          <Checkbox>Multiplayer</Checkbox>
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit" disabled={!isLoaded}>
            Submit
          </Button>
        </Form.Item>
        
      </Form>
    </Content>
  )
}

export default GameEditForm
