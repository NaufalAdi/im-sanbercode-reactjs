import React, {useState} from 'react'
import { Form, Input, Button, InputNumber, Checkbox } from 'antd';
import axios from 'axios';
import Cookies from 'js-cookie';
import { useHistory } from 'react-router';
import { Content } from 'antd/lib/layout/layout';

const GameCreateForm = () => {
  const [form] = Form.useForm()
  let history = useHistory()

  const handleSubmit = () => {
    var data = form.getFieldsValue()
    data.singlePlayer = +data.singlePlayer
    data.multiplayer = +data.multiplayer
    console.log(data)
    
    axios.post("https://backendexample.sanbersy.com/api/data-game", 
      data,
      {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}}
    ).then(
        () => {
            history.push('/game')
        }
    ).catch((err) => {
        alert("Invalid credentials")
        console.log(err)
    })
  }

  return (
    <Content className="content-home content">
      <h1> Create Game </h1>
      <Form
        form={form}
        name="game"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={handleSubmit}
        autoComplete="off"
      >
        <Form.Item
          label="Name"
          name="name"
          rules={[{ required: true }]}
        >
          <Input/>
        </Form.Item>

        <Form.Item
          label="Image URL"
          name="image_url"
          rules={[{ required: true }]}
        >
          <Input.TextArea autoSize/>
        </Form.Item>

        <Form.Item
          label="Genre"
          name="genre"
          rules={[{ required: true }]}
        >
          <Input/>
        </Form.Item>

        <Form.Item
          label="Release"
          name="release"
          rules={[{ required: true }]}
        >
          <InputNumber min={2000} max={2021}/>
        </Form.Item>

        <Form.Item
          label="Platform"
          name="platform"
          rules={[{ required: true }]}
        >
          <Input.TextArea autoSize/>
        </Form.Item>

        <Form.Item 
          valuePropName="checked"
          name="singlePlayer" 
          wrapperCol={{ offset: 8, span: 16 }}
        >
          <Checkbox>Singleplayer</Checkbox>
        </Form.Item>

        <Form.Item 
          valuePropName="checked"
          name="multiplayer" 
          wrapperCol={{ offset: 8, span: 16 }}
        >
          <Checkbox>Multiplayer</Checkbox>
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
        
      </Form>
    </Content>
  )
}

export default GameCreateForm
