import React, {useEffect, useState, useContext} from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'
import { message, Table, Button, Space, Form, Input, InputNumber, Divider } from 'antd'
import { DeleteFilled, EditOutlined, CheckOutlined, CloseOutlined } from '@ant-design/icons'
import '../movie/MovieList.css'
import Cookies from 'js-cookie'
import { Content } from 'antd/lib/layout/layout'
import { GameContext } from './GameContext'

const GameList = () => {
  const { games, setGames, fetchGames } = useContext(GameContext)
  // const [ games, setGames ] = useState([])
  const [ isSearching, setIsSearching ] = useState(false)
  const [ filteredData, setFilteredData ] = useState([])
  const [ searchedData, setSearchedData ] = useState([])
  const [ searchInput, setSearchInput ] = useState("")
  const [ filterForm ] = Form.useForm()

  const handleDelete = (event) => {
    let id = event.currentTarget.value
    axios.delete(`https://backendexample.sanbersy.com/api/data-game/${id}`,
    {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}}
  ).then(() => {
      let newGames = games.filter(el=> {return el.id !== parseInt(id)})
      setGames(newGames)
      setFilteredData(newGames)
      message.success("Item deleted successfully")
  }).catch((err) => {
      message.error(err.message)
      console.log(err)
  })
}

  const clearForm = () => {
    filterForm.resetFields()
    filterForm.submit()
  }

  const onFilter = () => {
    let query = filterForm.getFieldsValue()
    console.log(query)
    setIsSearching(false)
    setSearchInput("")
    let filteredData = games
    filteredData = games.filter(el => (
        (query.genre !== '' ? 
          el.genre.toLowerCase().includes(query.genre.toLowerCase()) : true
        ) && (query.platform !== '' ? 
          el.platform.toLowerCase().includes(query.platform.toLowerCase()) : true
        ) && (query.release !== null ? 
          parseInt(el.year) === query.release : true 
        )
    ))
    setFilteredData(filteredData)
  }

  const onTitleSearch = event => {
    let query = event.currentTarget.value
    setSearchInput(query)
    setIsSearching(true)
    setSearchedData(filteredData.filter(
      el => el.name.toLowerCase().includes(query.toLowerCase())
    ))
  }

  useEffect(() => {
    fetchGames()
  }, [])

  useEffect(() => {
    setFilteredData(games)
  }, [games])

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      width: 100,
      sorter: {
        compare: (a, b) => a.name.localeCompare(b.name),
        multiple: 1
      }
    },
    {
      title: 'Cover',
      dataIndex: 'image_url',
      key: 'image_url',
      width: 160,
      render: image_url => (
        <img style={{width:"150px"}} src={image_url} alt=""/>
      )
    },
    {
      title: 'Genre',
      dataIndex: 'genre',
      key: 'genre',
      width: 100,
      sorter: {
        compare: (a, b) => a.genre.localeCompare(b.genre),
        multiple: 1
      }
    },
    {
      title: 'Singleplayer',
      dataIndex: 'singlePlayer',
      key: 'singlePlayer',
      width: 120,
      sorter: {
        compare: (a, b) => a.singlePlayer - b.singlePlayer,
        multiple: 1
      },
      render: singlePlayer => (
        singlePlayer === 1 ? <CheckOutlined /> : <CloseOutlined />
      )
    },
    {
      title: 'Multiplayer',
      dataIndex: 'multiplayer',
      key: 'multiplayer',
      width: 120,
      sorter: {
        compare: (a, b) => a.multiPlayer - b.multiPlayer,
        multiple: 1
      },
      render: multiplayer => (
        multiplayer === 1 ? <CheckOutlined /> : <CloseOutlined />
      )
    },
    {
      title: 'Platform',
      dataIndex: 'platform',
      key: 'platform',
      width: 200,
      sorter: {
        compare: (a, b) => a.platform.localeCompare(b.platform),
        multiple: 1
      }
    },
    
    {
      title: 'Release',
      dataIndex: 'release',
      key: 'release',
      width: 100,
      sorter: {
        compare: (a, b) => parseInt(a.release) - parseInt(b.release),
        multiple: 1
      }
    },
    {
      title: 'Action',
      key: 'action',
      width: 140,
      render: (item) => (
        <Space size="middle">
            <Link to={`/game/edit/${item.id}`}>
                <Button type="warning" ><EditOutlined /></Button>
            </Link>
            <Button danger type="primary" onClick={handleDelete} value={item.id} ><DeleteFilled/></Button>
        </Space>
      ),
    },
  ];


  return (
    <Content>
      <div className="filter-container content">
      <Divider style={{marginTop: 0}} orientation="left">Filter</Divider>
        <Form
          form={filterForm}
          name="filterForm"
          onFinish={onFilter}
          labelCol= {{ xs: { span: 4 }, sm: { span: 4 }, md: { span: 4 }, lg: { span: 3 } }}
          wrapperCol = {{ xs: { span: 18 }, sm: { span: 12 }, md: { span: 12 }, lg: { span: 12 } }}
          initialValues= {{
            genre: "",
            platform: "",
            release: null
          }}
          >
          <Form.Item label="Genre"name="genre" >
            <Input />
          </Form.Item>

          <Form.Item label="Platform"name="platform">
            <Input />
          </Form.Item>

          <Form.Item label="Release"name="release">
              <InputNumber min={2000} max={2021}/>
              <div style={{float: "right", right: 0}}>
                <Space>
                <Button htmlType="submit">Submit</Button>
                <Button danger onClick={clearForm}>Clear</Button>
                </Space>
              </div>
          </Form.Item>
        </Form>
      </div>

      <div className="table-container content">
        <Button className="item-add-btn" type="primary">
          <Link to="/game/create" >+ Add Game</Link>
        </Button>
        <Input.Search 
          value={searchInput} 
          className="item-title-search"
           style={{ maxWidth:"240px"}}
           placeholder="Search title" 
           onChange={onTitleSearch} 
           allowClear={true}
        />
        <Table 
          columns={columns} 
          dataSource={isSearching ? searchedData: filteredData} 
          scroll={{ x: "100%"}} 
          rowKey="game"
        />
      </div>
    </Content>
  )
}

export default GameList
