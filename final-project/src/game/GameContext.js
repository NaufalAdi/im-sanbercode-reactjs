import { message } from 'antd';
import axios from 'axios';
import React, { createContext, useState } from 'react'
import { useHistory } from 'react-router';
export const GameContext = createContext();
export const GameProvider = props => {
  const [ games, setGames] = useState([])
  let history = useHistory()

  const fetchGames = async () => {
    console.log("Fetching Games")
    axios.get("https://backendexample.sanbersy.com/api/data-game")
      .then( res => {
        setGames(res.data)
      }).catch((err) => {
        message.error(err.message)
    })
  }

  const fetchGameById = async (id) => {
    try {
      console.log(`https://backendexample.sanbersy.com/api/data-game/${id}`)
      let res = await axios.get(`https://backendexample.sanbersy.com/api/data-game/${id}`)
        res.data.singlePlayer = !!res.data.singlePlayer
        res.data.multiplayer = !!res.data.multiplayer
        return res.data
    } catch(err) {
      message.error(err.message)
      history.goBack()
    }
  }

  return (
    <GameContext.Provider value={{
      games, 
      setGames,
      fetchGames,
      fetchGameById
      }}
    >
      {props.children}
    </GameContext.Provider>
  );
};
