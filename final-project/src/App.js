import 'antd/dist/antd.css'
import './App.css';
import { UserProvider } from './auth/UserContext';
import Main from './main/Main';

function App() {
  return (
    <UserProvider>
      <Main />
    </UserProvider>
  );
}

export default App;
