import axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import { useParams, useHistory } from 'react-router'
import { Row, Col, Spin, Tag, Space, Typography, Divider, message } from 'antd'
import { LoadingOutlined, StarFilled } from '@ant-design/icons';
import './Details.css'
import { Content } from 'antd/lib/layout/layout';
import { MovieContext } from '../movie/MovieContext';

const MovieDetails = () => {
  const { id } = useParams()
  const { fetchMovieById } = useContext(MovieContext)
  const [movie, setMovie] = useState()
  const [isLoaded, setIsLoaded] = useState(false)
  const antIcon = <LoadingOutlined style={{ fontSize: 90 }} spin />
  const { Paragraph, Text, Title } = Typography
  let history = useHistory()
  

  const init = async () => {
    try {
      let data = await fetchMovieById(id)
      setMovie(data)
      setIsLoaded(true)
    } catch(err) {
      message.error(err.message)
      history.push('/')
    }
  }

  const renderGenre = (genre) => {
    let condition = genre.trim().toLowerCase()
    let color = 
    condition === "action" ? "red" :
    condition === "romance" ? "magenta" :
    condition === "drama" ? "green" :
    condition === "comedy" ? "gold" :
    condition === "thriller" ? "grey" :
    condition === "horror" ? "black" : "cyan"
    return <Tag 
      color={color} 
      style={{marginLeft: "8px", minWidth: "70px", height:"25px", textAlign:"center", borderRadius: "10px"}}
    >
      {genre}
    </Tag>
  }

  useEffect(() => {
      init()
  }, [])

  return (
    <Content>
      <div className="content content-details">
      {isLoaded && 
        <div>
          <Row gutter={[{ xs: 4, md: 12, lg: 22}, { xs: 4, md: 12, lg: 18 }]}>
            <Col xs={24} lg={10}>
              <img class="details-img" src={movie.image_url} alt={movie.title}></img>
            </Col>
            <Col xs={24} lg={14}>
              <Title level={2} ellipsis="true">{movie.title}</Title>
              <Paragraph ellipsis="true">
                <StarFilled style={{color: "gold"}}/> <Text strong> {movie.rating}/10 </Text>
                {movie.genre.split(',').map(genre => renderGenre(genre))}
              </Paragraph>
              <Paragraph ellipsis="true" type="secondary">
                   {movie.duration} minutes ● {movie.year}
              </Paragraph>
              <Divider orientation="left">Description</Divider>
              <Text>
                   {movie.description}
              </Text>
              <Divider orientation="left">Review</Divider>
              <Text italic>
                   "{movie.review}"
              </Text>
            </Col>
          </Row>
        </div>
      }
      {!isLoaded &&
        <div>
          <Row >
            <Col xs={24} lg={10}>
              <Spin class="details-img"indicator={antIcon} />
            </Col>
            <Col xs={24} lg={14}>
              <h2>Loading</h2>
              <span><Tag>Loading</Tag><Tag>Loading</Tag></span>
            </Col>
          </Row>
        </div>
      }
      </div>
    </Content>

  )
}

export default MovieDetails
