import axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import { useParams, useHistory } from 'react-router'
import { Row, Col, Spin, Tag, Space, Typography, Divider, message } from 'antd'
import { LoadingOutlined, StarFilled } from '@ant-design/icons';
import './Details.css'
import { Content } from 'antd/lib/layout/layout';
import { GameContext } from '../game/GameContext';

const GameDetails = () => {
  const { id } = useParams()
  const [game, setGame] = useState({})
  const [isLoaded, setIsLoaded] = useState(false)
  const antIcon = <LoadingOutlined style={{ fontSize: 90 }} spin />
  const { Paragraph, Text, Title } = Typography
  const { fetchGameById } = useContext(GameContext)
  let history = useHistory()

  const init = async () => {
    try {
      let data = await fetchGameById(id)
      setGame(data)
      setIsLoaded(true)
    } catch {
      history.push('/')
    }
  }

  const renderGenre = (genre) => {
    let condition = genre.trim().toLowerCase()
    let color = 
    condition.includes("action") ? "red" :
    condition.includes("open world") ? "magenta" :
    condition.includes("moba")  ? "green" :
    condition === "adventrue" ? "gold" :
    condition === "survival" ? "purple" :
    condition === "horror" ? "black" : "cyan"
    return <Tag 
      color={color} 
      style={{marginLeft: "8px", minWidth: "70px", height:"25px", textAlign:"center", borderRadius: "10px"}}
    >
      {genre}
    </Tag>
  }

  useEffect(() => {
    init()
  },[])

  return (
    <Content>
      <div className="content content-details">
      {isLoaded && 
        <div>
          <Row gutter={[{ xs: 4, md: 12, lg: 22}, { xs: 4, md: 12, lg: 18 }]}>
            <Col xs={24} lg={10}>
              <img className="details-img" src={game.image_url} alt={game.name}></img>
            </Col>
            <Col xs={24} lg={14}>
              <Title level={2} ellipsis="true">{game.name}</Title>
              <Paragraph ellipsis="true">
                 <Text type="secondary"> {game.release} </Text>
                {game.genre.split(',').map(genre => renderGenre(genre))}
              </Paragraph>
              <Paragraph ellipsis="true" type="secondary">
                   {game.singlePlayer === true && "Singleplayer"}
                   {game.singlePlayer === true && "Multiplayer" && game.multiplayer === true && " ● "}
                   {game.multiplayer === true && "Multiplayer"}
              </Paragraph>
              <Text>
                   Platform: {game.platform}
              </Text>
            </Col>
          </Row>
        </div>
      }
      {!isLoaded &&
        <div>
          <Row >
            <Col xs={24} lg={10}>
              <Spin className="details-img"indicator={antIcon} />
            </Col>
            <Col xs={24} lg={14}>
              <h2>Loading</h2>
              <span><Tag>Loading</Tag><Tag>Loading</Tag></span>
            </Col>
          </Row>
        </div>
      }
      </div>
    </Content>

  )
}

export default GameDetails
