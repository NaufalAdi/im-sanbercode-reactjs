import 'antd/dist/antd.css'
import './App.css';
import Routes from './nav/Routes';

function App() {
  return (
    <Routes/>
  );
}

export default App;
