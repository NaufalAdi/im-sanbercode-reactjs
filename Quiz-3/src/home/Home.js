import React, { useEffect, useContext } from 'react';
import {Divider} from 'antd'
import { MobileAppContext } from '../mobileApp/MobileAppContext';

function Home() {
  const { states, functions } = useContext(MobileAppContext)
  const { arrApp } = states
  const { fetchData } = functions

  useEffect( () => {
    fetchData()
  }, [])

  const renderSize = size => {
    return size >= 1000 ? Math.round(size / 1000) + " GB":
      size + " MB"
  }

  return (
    <div className="section">
      <h1 className="title">Popular Mobile App</h1>
      {arrApp.map(item => {
        return(
          <div className="card">
            <div>
              <h2>{item.name}</h2>
              <h5>Release Year : {item.releaseYear}</h5>
              <img style={{width: "50%", height: "300px", objectFit: "cover"}} src={item.imageUrl} alt="app"/>
              <br />
              <br />
              <div>
                <strong>Price: {item.price === 0? "FREE" : "Rp"+item.price+",00"}</strong><br />
                <strong>Rating: {item.rating}</strong><br />
                <strong>Size: {renderSize(item.size)}</strong><br />
                <strong style={{marginRight: "10px"}}>Platform : Android & IOS
                </strong>
                <br />
              </div>
              <p>
                <strong style={{marginRight: "10px"}}>Description : </strong>
                {item.description}
              </p>
              <Divider/>
            </div>
        </div>
        )
      })}
    </div>
  )
}

export default Home
