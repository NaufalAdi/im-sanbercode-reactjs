import React, { useEffect, useContext } from 'react'
import { MobileAppContext } from '../mobileApp/MobileAppContext'

function Search() {
    const { states, functions } = useContext(MobileAppContext)
    const { arrApp } = states
    const { fetchData } = functions

    useEffect(() =>{
        fetchData()
    }, [])

    const submitHandler = () => {
       
    }
    
    return (
        <form onSubmit={submitHandler}>
            <input type="text" />
            <input type="submit" value="Cari" />
        </form>
    )
}

export default Search
