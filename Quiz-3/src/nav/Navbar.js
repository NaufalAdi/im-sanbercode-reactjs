import React from 'react'
import logo from './logo.png'
import Search from './Search'

function Navbar() {
    return (
        <div className="topnav">
        <img id="logo" src={logo} width="200px" alt="sanber-logo"/>
        <a href="/">Home</a>
        <a href="/mobile-list">Mobile List</a>
        <a href="/about">About</a>
        <Search></Search>
    </div>
    )
}

export default Navbar
