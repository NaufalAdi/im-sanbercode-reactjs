import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import About from '../about/About';
import Home from '../home/Home';
import { MobileAppProvider } from '../mobileApp/MobileAppContext';
import MobileAppForm from '../mobileApp/MobileAppForm';
import MobileAppList from '../mobileApp/MobileAppList';
import Footer from './Footer';
import Navbar from './Navbar';
import Search from './Search';
  
function Routes() {
  return (
    <Router>
      <MobileAppProvider>
      <Navbar />
      <div className="row">
        <Switch>
          
            <Route exact path="/" component={Home} />
            <Route exact path="/mobile-list" component={MobileAppList} />
            <Route exact path="/mobile-form/edit/:id" component={MobileAppForm} />
            <Route exact path="/mobile-form" component={MobileAppForm} />
            <Route exact path="/search/:valueOfSearch" component={Search} />
            <Route exact path="/about" component={About} />
          
        </Switch>
      </div>
      </MobileAppProvider>
      <Footer />
    </Router>
  )
}

export default Routes
