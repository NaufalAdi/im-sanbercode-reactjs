import React, { useEffect, useContext } from 'react';
import { Link } from "react-router-dom";
import { message, Button, Table, Tag, Space } from 'antd'
import { DeleteFilled, EditFilled } from '@ant-design/icons'
import { MobileAppContext } from './MobileAppContext';

const MobileAppList = () => {
  const { states, functions } = useContext(MobileAppContext)
  const { arrApp } = states

  const {
      fetchData,
      deleteData,
      functionEdit,
      clearForm
  } = functions

  useEffect( () => {
    fetchData()
  }, [])

  const handleCreate = () => {
      clearForm()
  }

  const handleDelete = (event) => {
    let id = parseInt(event.currentTarget.value)
    deleteData(id)
    message.success('Data berhasil dihapus!')
  }

  const handleEdit = (event) => {
    let id = event.currentTarget.value
    functionEdit(id)
  }

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Category',
      dataIndex: 'category',
      key: 'category',
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
      ellipsis: true
    },
    {
      title: 'Release Year',
      dataIndex: 'releaseYear',
      key: 'releaseYear',
      render: releaseYear => releaseYear < 2007 ? 2007: releaseYear 
    },
    {
      title: 'Size',
      dataIndex: 'size',
      key: 'size',
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
    },
    {
      title: 'Rating',
      dataIndex: 'rating',
      key: 'rating',
    },
    {
      title: 'Platform',
      key: 'platform',
      render: (item) => {
        <p>
        {item.isAndroidApp === 1? <Tag color="green">Android</Tag> : null}
        {item.isIosApp === 1? <Tag color="geekblue">iOS</Tag> : null}
        </p>
      }
    },
    {
      title: 'Action',
      key: 'action',
      render: (item) => (
        <Space size="middle">
            <Link to={`/mobile-form/edit/${item.id}`}>
                <Button style={{ "backgroundColor": "yellow"}} type="warning" ><EditFilled style={{"color": "white"}}/></Button>
            </Link>
            <Button danger type="primary" onClick={handleDelete} value={item.id} ><DeleteFilled/></Button>
        </Space>
      ),
    },
  ];

  return (
    <div className="section">
      <h1>Mobile Apps List</h1>
      <Link to="/mobile-form"><Button type="primary" onClick={handleCreate}>Add an App</Button></Link>
      <Table columns={columns} dataSource={arrApp} />
  </div>
  )
}

export default MobileAppList
