import axios from "axios"
import React, { useState, createContext } from "react"

export const MobileAppContext = createContext()

export const MobileAppProvider = props => {
    const [arrApp, setArrApp] = useState([])
    const [input, setInput] = useState({
    })
    const [currentId, setCurrentId] = useState(-1)

    const fetchData = async () => {
        const result = await axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps`)
        setArrApp(result.data.map(data =>{
            return {
                id: data.id,
                name: data.name,
                category: data.category,
                description: data.description,
                releaseYear: data.release_year,
                size: data.size,
                price: data.price,
                rating: data.rating,
                imageUrl: data.image_url,
                isAndroidApp: data.is_android_app,
                isIosApp: data.is_ios_app
            } 
        }))
    }

    const createData = () => {
        axios.post(`http://backendexample.sanbercloud.com/api/mobile-apps`, {
            name: input.name,
            category: input.category,
            description: input.description,
            release_year: input.releaseYear,
            size: input.size,
            price: input.price,
            rating: input.rating,
            image_url: input.imageUrl,
            is_android_app: input.isAndroidApp,
            is_ios_app: input.isIosApp
        })
        .then(res => {
            let data = res.data
            setArrApp([...arrApp, 
                {   
                    id: data.id,
                    ...input
                }
            ])
        }).catch(error => console.log(error))
    }

    const updateData = async () => {
        axios.put(`http://backendexample.sanbercloud.com/api/mobile-apps/${currentId}`, {
            name: input.name,
            category: input.category,
            description: input.description,
            release_year: input.releaseYear,
            size: input.size,
            price: input.price,
            rating: input.rating,
            image_url: input.imageUrl,
            is_android_app: input.isAndroidApp,
            is_ios_app: input.isIosApp
        })
        .then(() => {
            let app = arrApp.find(el=> el.id === currentId)
            setArrApp({...app, ...input})
            setArrApp([...arrApp])
        })
    }

    const deleteData = async (id) => {
        axios.delete(`http://backendexample.sanbercloud.com/api/mobile-apps/${id}`)
            .then(() => {
                let newArrStudent = arrApp.filter(el=> {return el.id !== id})
                setArrApp(newArrStudent)
        })
    }

    const functionEdit = async (id) => {
        console.log(`edit ${id}`)
        axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps/${id}`)
        .then(res => {
            let data = res.data
            console.log(data)
            setInput({
                name: data.name,
                category: data.category,
                description: data.description,
                releaseYear: data.release_year,
                size: data.size,
                price: data.price,
                rating: data.rating,
                imageUrl: data.image_url,
                isAndroidApp: data.is_android_app,
                isIosApp: data.is_ios_app
                } )
            setCurrentId(data.id)

          })
    }

    const clearForm =  () => {
        setInput({
            name: "",
            category: "",
            description: "",
            releaseYear: "",
            size: "",
            price: "",
            rating: "",
            isAndroidApp: "",
            isIosApp: "",
            imageUrl: ""
        })
        setCurrentId(-1)
    }

    const functions = {
        fetchData,
        createData,
        updateData,
        deleteData,
        functionEdit,
        clearForm
    }

    const states = {
        arrApp, 
        setArrApp,
        input,
        setInput,
        currentId,
        setCurrentId,
    }

    return (
        <MobileAppContext.Provider value={{
            states,
            functions
        }}>
          {props.children}
        </MobileAppContext.Provider>
    );
}