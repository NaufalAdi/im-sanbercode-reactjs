import React, { useContext, useEffect  } from 'react';
import { Link, useHistory, useParams } from 'react-router-dom';
import { message, Form, Input, Button, Checkbox } from 'antd'
import { MobileAppContext } from './MobileAppContext';
import TextArea from 'rc-textarea';

function MobileAppForm() {
    const history = useHistory();
    const { states, functions } = useContext(MobileAppContext)
    let { id } = useParams()

    const {
        input,
        setInput,
        currentId,
        setCurrentId,
        
    } = states

    const {
        createData,
        updateData,
        functionEdit
    } = functions

    const handleChange = (event) => {
      console.log(event.currentTarget.name)
      console.log(event.currentTarget.value)
        setInput({
            ...input,
            [event.currentTarget.name]: event.currentTarget.value
            })
    }

    useEffect(() => {
      if (id !== null ){
        functionEdit(id)
        console.log(input)
      }
    }, [id])

    const handleSubmit = () => {
        //event.preventDefault()
        console.log(input)
        if (currentId === -1){
            createData()
            message.success('Data berhasil ditambah!')
        }else{
            updateData()
            message.success('Data berhasil diubah!')
        }
        setInput({
          name: "",
          category: "",
          description: "",
          releaseYear: "",
          size: "",
          price: "",
          rating: "",
          isAndroidApp: "",
          isIosApp: "",
          imageUrl: ""
      })
      setCurrentId(-1)
      history.push("/mobile-list")
    }

    const onCheckboxChange = (event) => {
      if (event.target.checked === true) {
        setInput({
          ...input,
          [event.currentTarget.name]: false
          })
          event.target.checked = false
      } else {
        setInput({
          ...input,
          [event.currentTarget.name]: true
          })
          event.target.checked = true
      }
    }

    return (
        <div className="section">
          <div className="card">
            <form class="mobile-form"onSubmit={handleSubmit}>
              <label>Name:</label>          
              <input type="text" name="name" value={input.name} onChange={handleChange} required/><br/><br/>
              
              <label>Category:</label>          
              <input type="text" name="category" value={input.category} onChange={handleChange} required/><br/><br/>
              
              <label>Description:</label>          
              <TextArea name="description" value={input.description} onChange={handleChange} required/><br/><br/>
              
              <label>Release Year:</label>          
              <input type="number" name="releaseYear" min={2007} value={input.releaseYear} onChange={handleChange} required/><br/><br/>
              
              <label>Size (MB):</label>          
              <input type="number" name="size" value={input.size} onChange={handleChange} required/><br/><br/>
              
              <label>Price:</label>          
              <input type="number" name="price" value={input.price} onChange={handleChange} required/><br/><br/>

              <label>Rating:</label>          
              <input type="number" name="rating"  value={input.rating} onChange={handleChange} required/><br/><br/>

              <label>Image URL:</label>          
              <input type="text" name="imageUrl" value={input.imageUrl} onChange={handleChange} required/><br/><br/>

              <input type="checkbox" checked={input.isAndroidApp===1?true:false} id="isAndroidApp" name="isAndroidApp" onChange={onCheckboxChange}/>
              <p for="isAndroidApp"> Android </p><br/>
              <input type="checkbox" checked={input.isIosApp===1?true:false} id="isIosApp" name="isIosApp" onChange={onCheckboxChange} />
              <p for="isIosApp"> iOS</p><br/>
              <input type="submit" value="Submit" />

            <Link to="/mobile-list">Kembali ke tabel</Link>
        </form>
          </div>
     
 
      </div>
  )
}

export default MobileAppForm
