// Soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

kataKedua = kataKedua[0].toUpperCase() + kataKedua.slice(1)
kataKetiga = kataKetiga.slice(0,6) + kataKetiga[6].toUpperCase() 
kataKeempat = kataKeempat.toUpperCase()

console.log(kataPertama + " " + kataKedua + " " + kataKetiga + " " + kataKeempat)

// Soal 2
var panjangPersegiPanjang = "8";
var lebarPersegiPanjang = "5";

var alasSegitiga= "6";
var tinggiSegitiga = "7";

var kelilingPersegiPanjang;
var luasSegitiga;

kelilingPersegiPanjang = 2 * (parseInt(panjangPersegiPanjang) + parseInt(lebarPersegiPanjang));
luasSegitiga = (alasSegitiga * tinggiSegitiga) / 2;
console.log("Keliling persegi panjang: " + kelilingPersegiPanjang);
console.log("Luas segitiga: " + luasSegitiga);

//Soal 3
var sentences = 'wah javascript itu keren sekali'; 

var firstWord = sentences.substring(0, 3);
var secondWord = sentences.substring(4, 14);
var thirdWord = sentences.substring(15, 18);
var fourthWord = sentences.substring(19, 24);
var fifthWord = sentences.substring(25); 
console.log('Kata Pertama: ' + firstWord); 
console.log('Kata Kedua: ' + secondWord); 
console.log('Kata Ketiga: ' + thirdWord); 
console.log('Kata Keempat: ' + fourthWord); 
console.log('Kata Kelima: ' + fifthWord);

//Soal 4
var nilaiJohn = 80;
var nilaiDoe = 50;

if (nilaiJohn >= 80) {
    console.log("Nilai John: A")
} else if (nilaiJohn >= 70) {
    console.log("Nilai John: B")
} else if (nilaiJohn >= 60) {
    console.log("Nilai John: C")
} else if (nilaiJohn >= 50) {
    console.log("Nilai John: D")
} else {
    console.log("Nilai John: E")
}

if (nilaiDoe >= 80) {
    console.log("Nilai Doe: A")
} else if (nilaiDoe >= 70) {
    console.log("Nilai Doe: B")
} else if (nilaiDoe >= 60) {
    console.log("Nilai Doe: C")
} else if (nilaiDoe >= 50) {
    console.log("Nilai Doe: D")
} else {
    console.log("Nilai Doe: E")
}

//Soal 5
var tanggal = 14;
var bulan = 11;
var tahun = 2000;
var namaBulan;
switch(bulan){
    case 1: namaBulan = "Januari"; break;
    case 2: namaBulan = "Februari"; break;
    case 3: namaBulan = "Maret"; break;
    case 4: namaBulan = "April"; break;
    case 5: namaBulan = "Mei"; break;
    case 6: namaBulan = "Juni"; break;
    case 7: namaBulan = "Juli"; break;
    case 8: namaBulan = "Agustus"; break;
    case 9: namaBulan = "September"; break;
    case 10: namaBulan = "Oktober"; break;
    case 11: namaBulan = "November"; break;
    case 12: namaBulan = "Desember"; break;
}

console.log(tanggal + " " + namaBulan + " " + tahun)