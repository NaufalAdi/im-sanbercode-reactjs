var filterBooksPromise = require('./promise2.js')

filterBooksPromise(true, 40).then(
    result => console.log(result)
).catch(
    error => console.log(error)
)

const execute = async (colorful, amountOfPage) => {
    try{
        const result = await filterBooksPromise(colorful, amountOfPage)
        console.log(result)
    } catch(err) {
        console.log(err.message)
    }
}

execute(false, 250)
execute(true, 30)