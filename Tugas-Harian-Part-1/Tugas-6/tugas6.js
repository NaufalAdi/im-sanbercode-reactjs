//soal 1
console.log("----soal 1----")
const luasLingkaran = r => {
    const pi = 22/7
    return pi * r * r
}
const kelilingLingkaran = r => {
    const pi = 22/7
    return 2 * pi * r
}

console.log(luasLingkaran(7))
console.log(kelilingLingkaran(7))
console.log()

//soal 2
console.log("----soal 2----")
const introduce = (name, age, gender, occupation) => {
    let honorific = gender === "Laki-Laki" ? "Pak" : "Bu"
    return `${honorific} ${name} adalah seorang ${occupation} yang berusia ${age} tahun`
}
const perkenalan = introduce("John", "30", "Laki-Laki", "penulis")
console.log(perkenalan)
console.log()

//soal 3
console.log("----soal 3----")
const newFunction = function literal(firstName, lastName){
    return {
      firstName,
      lastName,
      fullName: () => console.log(`${firstName} ${lastName}`)
      
    }
  }
    
// kode di bawah ini jangan diubah atau dihapus sama sekali
  console.log(newFunction("John", "Doe").firstName)
  console.log(newFunction("Richard", "Roe").lastName)
  newFunction("William", "Imoh").fullName()
  console.log()

//soal 4
console.log("----soal 4----")
let phone = {
    name: "Galaxy Note 20",
    brand: "Samsung",
    year: 2020,
    colors: ["Mystic Bronze", "Mystic White", "Mystic Black"]
 }

let {name, brand, year, colors} = phone
let phoneBrand = brand
let phoneName = name
let [colorBronze, colorWhite, colorBlack] = colors
console.log(phoneBrand, phoneName, year, colorBlack, colorBronze) 
console.log()

//soal 5
console.log("-----soal 5-----")
let warna = ["biru", "merah", "kuning" , "hijau"]

let dataBukuTambahan= {
  penulis: "john doe",
  tahunTerbit: 2020 
}

let buku = {
  nama: "pemograman dasar",
  jumlahHalaman: 172,
  warnaSampul:["hitam"]
}

buku.warnaSampul = [...buku.warnaSampul, ...warna]
buku = {...buku, ...dataBukuTambahan}
console.log(buku)