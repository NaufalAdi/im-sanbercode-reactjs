//Soal1
console.log("LOOPING PERTAMA")
var i = 0
while (i < 20){
    i+=2
    console.log(i + " - I love coding")
} 
console.log("LOOPING KEDUA")
while (i > 0){
    console.log(i + " -  I will become a frontend developer")
    i-=2
}
//Soal 2
for (var i = 1; i <= 20; i++){
    var string
    if (i % 2 === 0){
        string = "Berkualitas"
    } else {
        if (i % 3 === 0) {
            string = "I Love Coding"
        } else {
            string = "Santai"
        }
    }
    console.log(i + " - " + string)
}
//Soal 3
for (var i = 1; i <= 7; i ++){
    var string = ""
    for (var j = 1; j <= i; j++){
        string = string.concat("#")
    }
    console.log(string)
}
//Soal 4
var kalimat=["aku", "saya", "sangat", "sangat", "senang", "belajar", "javascript"]
kalimat.splice(0, 1)
kalimat.splice(1, 1)
console.log(kalimat.join(" "))
//Soal 5
var sayuran = []
sayuran.push("Kangkung")
sayuran.push("Bayam")
sayuran.push("Buncis")
sayuran.push("Kubis")
sayuran.push("Timun")
sayuran.push("Seledri")
sayuran.push("Tauge")
sayuran.sort()
for (var i = 0; i < sayuran.length; i++){
    console.log((i + 1) + ". " + sayuran[i])
}