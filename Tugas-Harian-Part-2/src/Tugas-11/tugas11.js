import React, { useState, useEffect } from 'react';
import './tugas11.css'

const Tugas11 = () => {
    const [daftarBuah, setDaftarBuah] = useState([
        {nama: "Nanas", hargaTotal: 100000, beratTotal: 4000 },
        {nama: "Manggis", hargaTotal: 350000, beratTotal: 10000},
        {nama: "Nangka", hargaTotal: 90000, beratTotal: 2000},
        {nama: "Durian", hargaTotal: 400000, beratTotal: 5000},
        {nama: "Strawberry", hargaTotal: 120000, beratTotal: 6000}
      ])
    const [inputNama, setInputNama] =  useState("")
    const [inputHarga, setInputHarga] =  useState("")
    const [inputBerat, setInputBerat] =  useState("")
    const [currentIndex, setCurrentIndex] = useState(-1)

    const handleNamaChange = (event) => {
        setInputNama(event.target.value)
    }

    const handleBeratChange = (event) => {
        setInputBerat(event.target.value)
    }

    const handleHargaChange = (event) => {
        setInputHarga(event.target.value)
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        let buah = {
            nama: inputNama,
            hargaTotal: parseInt(inputHarga),
            beratTotal: parseInt(inputBerat)
        }
        if (currentIndex === -1) {
            setDaftarBuah([...daftarBuah, buah])
        } else {
            daftarBuah[currentIndex] = buah
            setCurrentIndex(-1)
        }
        setInputNama("")
        setInputBerat("")
        setInputHarga("")
    }

    const handleDelete = (event) => {
        let index = parseInt(event.target.value)
        let deletedItem = daftarBuah[index]
        let newData = daftarBuah.filter((e) => {return e !== deletedItem})
        setDaftarBuah(newData)
    }

    const handleEdit = (event) => {
        let index = parseInt(event.target.value)
        let editValue = daftarBuah[index]
        setInputNama(editValue.nama)
        setInputBerat(editValue.beratTotal)
        setInputHarga(editValue.hargaTotal)
        setCurrentIndex(event.target.value)
    }

    return (
        <div className="tugas11">
        <h1>Daftar Harga Buah</h1>
        <table>
            <thead>
                <tr>
                <th className="no">No</th>
                <th>Nama</th>
                <th>Harga total</th>
                <th>Berat total</th>
                <th>Harga per kg</th>
                <th>Aksi</th>
                </tr>
            </thead>
            <tbody>

                {
                daftarBuah.map((val, index) => {
                    return (
                    <tr>
                        <td className="no">{index + 1}</td>
                        <td>{val.nama}</td>
                        <td>{val.hargaTotal}</td>
                        <td>{val.beratTotal/1000} kg</td>
                        <td>{val.hargaTotal/(val.beratTotal/1000)}</td>
                        <td>
                            <button onClick={handleEdit} value={index}>Edit</button>
                            <button onClick={handleDelete} value={index}>Delete</button>
                        </td>
                    </tr>
                    )
                })
                }
            </tbody>
        </table>
        <h1>Form Daftar Harga Buah</h1>
        <form onSubmit={handleSubmit}>
            <label>Nama:</label>          
            <input type="text" value={inputNama} onChange={handleNamaChange} required/><br/><br/>
            <label>Harga Total:</label>          
            <input type="number" value={inputHarga} onChange={handleHargaChange} required/><br/><br/>
            <label>Berat Total(dalam gram):</label>          
            <input type="number" value={inputBerat} onChange={handleBeratChange} min="2000" required/><br/><br/>
            <input type="submit" value="Submit" />
        </form>
        </div>
      )
}

export default Tugas11