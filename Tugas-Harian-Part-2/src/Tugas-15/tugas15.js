import React  from 'react'
import { StudentProvider } from '../Tugas-14/StudentContext'
import StudentForm from './StudentForm'
import StudentList from './StudentList'

import {
    Switch,
    Route,
  } from "react-router-dom";

function Tugas15() {
    return (
        <StudentProvider>
            <Switch>
                <Route  exact path="/tugas15/create" component={StudentForm} />
                <Route  exact path="/tugas15/update" component={StudentForm} />
                <Route  exact path="/tugas15" component={StudentList} />
            </Switch>
        </StudentProvider>
    )
}

export default Tugas15
