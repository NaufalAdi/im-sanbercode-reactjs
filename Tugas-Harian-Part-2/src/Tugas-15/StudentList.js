import React, { useEffect, useContext } from 'react';
import { StudentContext } from '../Tugas-14/StudentContext';
import { Link } from "react-router-dom";
import { message, Button, Table, Space } from 'antd'
import { DeleteFilled, EditFilled } from '@ant-design/icons'

const StudentList = () => {
    const { states, functions } = useContext(StudentContext)
    const { arrStudent } = states

    const {
        fetchData,
        deleteData,
        functionEdit,
        clearForm
    } = functions

    useEffect( () => {
        fetchData()
    }, [])

    const handleCreate = () => {
        clearForm()
    }

    const handleDelete = (event) => {
        let id = parseInt(event.currentTarget.value)
        deleteData(id)
        message.success('Data berhasil dihapus!')
    }

    const handleEdit = (event) => {
        let id = event.currentTarget.value
        functionEdit(id)
    }

    const letterGrade = (score) => {
        if (score >= 80) return "A"
        else if (score >= 70) return "B"
        else if (score >= 60) return "C"
        else if (score >= 50) return "D"
        else return "E"
    }

    const columns = [
        {
          title: 'Name',
          dataIndex: 'name',
          key: 'name',
        },
        {
          title: 'Course',
          dataIndex: 'course',
          key: 'course',
        },
        {
          title: 'Score',
          dataIndex: 'score',
          key: 'score',
        },
        {
        title: 'Index Score',
        dataIndex: 'score',
        key: 'score',
        render: score => letterGrade(score)
        },
        {
          title: 'Action',
          key: 'action',
          render: (item) => (
            <Space size="middle">
                <Link to="/tugas15/update">
                    <Button style={{ "background-color": "yellow"}} type="warning" onClick={handleEdit} value={item.id} ><EditFilled style={{"color": "white"}}/></Button>
                </Link>
                <Button danger type="primary" onClick={handleDelete} value={item.id} ><DeleteFilled/></Button>
            </Space>
          ),
        },
      ];
    return (
        <div className="tugas11">
            <h1>Daftar Nilai Mahasiswa</h1>
            <Link to="/tugas15/create"><Button type="primary" onClick={handleCreate}>Buat Data Nilai Mahasiswa Baru</Button></Link>
            <Table columns={columns} dataSource={arrStudent} />
            
        </div>
      )
}

export default StudentList