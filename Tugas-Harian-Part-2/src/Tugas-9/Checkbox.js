const Checkbox = (props) => {
    return (
      <div className="container-checkbox">
        <label className="checkbox"> {props.text}
            <input type="checkbox"/>
            <span className="checkmark"></span>
        </label>
        <hr></hr>
      </div>
    )
}
export default Checkbox