import logo from './logo.png';
import Checkbox from "./Checkbox"

const Tugas9 = () => {
    return (
      <div className="container">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 id="tugas-9-header">THINGS TO DO</h1>
        <p>During bootcamp in sanbercode</p>
        <hr></hr>
        <Checkbox text="Belajar GIT & CLI"></Checkbox>
        <Checkbox text="Belajar HTML & CSS"></Checkbox>
        <Checkbox text="Belajar Javascript"></Checkbox>
        <Checkbox text="Belajar ReactJS Dasar"></Checkbox>
        <Checkbox text="Belajar ReactJs Advance"></Checkbox>
        <input type="submit" value="SEND" className="checkbox-submit"></input>
      </div>
    );
}

export default Tugas9