import React, { useState, useEffect } from 'react';

const Tugas10 = () => {
    const [counter, setCounter] = useState(100)
    const [showComponent, setShowComponent] = useState(true)

    useEffect(() => {
        if (counter !== 0) {
            setTimeout(() => setCounter(counter - 1), 1000)
        } else {
            setShowComponent(false)
        }
    },[counter, showComponent])
    
    return(
        <div className="clock">
            {showComponent ? (
                <>
                    <h1>Now At - {clock()}</h1>
                    <h2>Countdown: {counter}</h2>
                </>
            ): null}
        </div>
    )
}
// Dari tugas 7
const clock = () => {
    var date = new Date();

    var hours = date.getHours();

    var period
    if (hours < 12) {
        period = "AM"
    } else {
        period = "PM"
        hours = hours - 12
    }

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = "h:m:s "
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

    output += period
    return output;
}

export default Tugas10
