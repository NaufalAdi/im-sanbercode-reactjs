import { StudentProvider } from './StudentContext'
import StudentForm from './StudentForm'
import StudentList from './StudentList'
import '../Tugas-11/tugas11.css'

function Tugas13() {
    return (
        <>
            <StudentProvider>
                <StudentList></StudentList>
                <StudentForm></StudentForm>
            </StudentProvider>
        </>
    )
}

export default Tugas13
  