import React, { useContext } from 'react';
import { StudentContext } from './StudentContext';

function StudentForm() {
    const { states, functions } = useContext(StudentContext)

    const {
        input,
        setInput,
        currentId,
        setCurrentId,
    } = states

    const {
        createData,
        updateData, 
    } = functions

    const handleChange = (event) => {
        setInput({
            ...input,
            [event.target.name]: event.target.value
            })
    }

    const handleSubmit = (event) => {
        event.preventDefault()

        if (currentId === null){
            createData()
        }else{
            updateData()
        }
        setInput({
            name: "",
            course: "",
            score: ""
        })
        setCurrentId(null)
    }


    return (
        <div className="tugas11">
            <h1>Form Nilai Mahasiswa</h1>
            <form onSubmit={handleSubmit}>
                <label>Nama:</label>          
                <input type="text" name="name" value={input.name} onChange={handleChange} required/><br/><br/>
                <label>Mata Kuliah:</label>          
                <input type="text" name="course" value={input.course} onChange={handleChange} required/><br/><br/>
                <label>Nilai:</label>          
                <input type="number" name="score" value={input.score} onChange={handleChange} required/><br/><br/>
                <input type="submit" value="Submit" />
            </form>
        </div>
    )
}

export default StudentForm
