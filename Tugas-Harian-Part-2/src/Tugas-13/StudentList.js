import React, { useEffect, useContext } from 'react';
import { StudentContext } from './StudentContext';

const StudentList = () => {
    const { states, functions } = useContext(StudentContext)

    const { arrStudent } = states

    const {
        fetchData,
        deleteData,
        functionEdit
    } = functions

    useEffect( () => {
        fetchData()
    }, [])

    const handleDelete = (event) => {
        let id = parseInt(event.target.value)
        deleteData(id)
    }

    const handleEdit = (event) => {
        let id = event.target.value
        functionEdit(id)
    }

    const letterGrade = (score) => {
        if (score >= 80) return "A"
        else if (score >= 70) return "B"
        else if (score >= 60) return "C"
        else if (score >= 50) return "D"
        else return "E"
    }
    return (
        <div className="tugas11">
            <h1>Daftar Nilai Mahasiswa</h1>
            <table>
                <thead>
                    <tr>
                    <th className="no">No</th>
                    <th>Nama</th>
                    <th>Mata Kuliah</th>
                    <th>Nilai</th>
                    <th>Indeks Nilai</th>
                    <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>

                    {
                    arrStudent.map((item, index) => {
                        return (
                        <tr>
                            <td className="no">{index + 1}</td>
                            <td>{item.name}</td>
                            <td>{item.course}</td>
                            <td>{item.score}</td>
                            <td>{letterGrade(item.score)}</td>
                            <td>
                                <button onClick={handleEdit} value={item.id}>Edit</button>
                                <button onClick={handleDelete} value={item.id}>Delete</button>
                            </td>
                        </tr>
                        )
                    })
                    }
                </tbody>
            </table>
        </div>
      )
}

export default StudentList