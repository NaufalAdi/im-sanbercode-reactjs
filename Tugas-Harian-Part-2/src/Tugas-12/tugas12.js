import React, { useState, useEffect } from 'react';
import axios from 'axios'
import '../Tugas-11/tugas11.css'

const Tugas12 = () => {
    const [listMhs, setListMhs] = useState([])
    const [input, setInput] = useState({})
    const [currentId, setCurrentId] = useState(null)

    const handleChange = (event) => {
        setInput({
            ...input,
            [event.target.name]: event.target.value
            })
        console.log(input)
    }

    useEffect( () => {
        const fetchData = async () => {
            const result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)
            setListMhs(result.data.map(x=>{
                return {
                    id: x.id,
                    name: x.name,
                    course: x.course,
                    score: x.score
                } 
            }))
        }
        fetchData()
    }, [])

    const handleSubmit = (event) => {
        event.preventDefault()

        if (currentId === null){
          // untuk create data baru
          axios.post(`http://backendexample.sanbercloud.com/api/student-scores`, input)
          .then(res => {
              let data = res.data
              setListMhs([...listMhs, 
                {   
                    id: data.id,
                    name: data.name,
                    course: data.course,
                    score: data.score
                }
            ])
          })
        }else{
          axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentId}`, input)
          .then(() => {
              let mahasiswa = listMhs.find(el=> el.id === currentId)
              mahasiswa.name = input.name
              mahasiswa.course = input.course
              mahasiswa.score = input.score
              setListMhs([...listMhs])
          })      
        }
        setInput({
            name: "",
            course: "",
            score: ""
        })
        setCurrentId(null)
    }

    const handleDelete = (event) => {
        let id = parseInt(event.target.value)
        axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${id}`)
        .then(() => {
          let newListMhs = listMhs.filter(el=> {return el.id !== id})
          setListMhs(newListMhs)
        })
    }

    const handleEdit = (event) => {
        let id = event.target.value
        axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${id}`)
        .then(res => {
            let data = res.data
            console.log(data)
            setInput({
                id: data.id,
                name: data.name,
                course: data.course,
                score: data.score
                } )
            setCurrentId(data.id)
          })
    }

    const letterGrade = (score) => {
        if (score >= 80) return "A"
        else if (score >= 70) return "B"
        else if (score >= 60) return "C"
        else if (score >= 50) return "D"
        else return "E"
    }
    return (
        <div className="tugas11">
        <h1>Daftar Nilai Mahasiswa</h1>
        <table>
            <thead>
                <tr>
                <th className="no">No</th>
                <th>Nama</th>
                <th>Mata Kuliah</th>
                <th>Nilai</th>
                <th>Indeks Nilai</th>
                <th>Aksi</th>
                </tr>
            </thead>
            <tbody>

                {
                listMhs.map((item, index) => {
                    return (
                    <tr>
                        <td className="no">{index + 1}</td>
                        <td>{item.name}</td>
                        <td>{item.course}</td>
                        <td>{item.score}</td>
                        <td>{letterGrade(item.score)}</td>
                        <td>
                            <button onClick={handleEdit} value={item.id}>Edit</button>
                            <button onClick={handleDelete} value={item.id}>Delete</button>
                        </td>
                    </tr>
                    )
                })
                }
            </tbody>
        </table>
        <h1>Form Nilai Mahasiswa</h1>
        <form onSubmit={handleSubmit}>
            <label>Nama:</label>          
            <input type="text" name="name" value={input.name} onChange={handleChange} required/><br/><br/>
            <label>Mata Kuliah:</label>          
            <input type="text" name="course" value={input.course} onChange={handleChange} required/><br/><br/>
            <label>Nilai:</label>          
            <input type="number" name="score" value={input.score} onChange={handleChange} required/><br/><br/>
            <input type="submit" value="Submit" />
        </form>
        </div>
      )
}

export default Tugas12