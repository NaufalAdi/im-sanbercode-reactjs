import React, { useContext } from 'react'
import {
    Link
} from "react-router-dom";
import { ThemeContext } from './ThemeContext';

function Navbar() {
    const{ theme } = useContext(ThemeContext)

    return (
        <nav className={theme}>
            <ul>
                <li><Link to="/">Tugas 9</Link></li>
                <li><Link to="/tugas10">Tugas 10</Link></li>
                <li><Link to="/tugas11">Tugas 11</Link></li>
                <li><Link to="/tugas12">Tugas 12</Link></li>
                <li><Link to="/tugas13">Tugas 13</Link></li>
                <li><Link to="/tugas14">Tugas 14</Link></li>
                <li><Link to="/tugas15">Tugas 15</Link></li>
            </ul>
            <hr></hr>
        </nav>
    )
}

export default Navbar
