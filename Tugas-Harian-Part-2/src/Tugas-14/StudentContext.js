import axios from "axios"
import React, { useState, createContext } from "react"

export const StudentContext = createContext()

export const StudentProvider = props => {
    const [arrStudent, setArrStudent] = useState([])
    const [input, setInput] = useState({})
    const [currentId, setCurrentId] = useState(null)

    const fetchData = async () => {
        const result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)
        setArrStudent(result.data.map(x=>{
            console.log("Data fetched")
            return {
                id: x.id,
                name: x.name,
                course: x.course,
                score: x.score
            } 
        }))
    }

    const createData = async () => {
        axios.post(`http://backendexample.sanbercloud.com/api/student-scores`, input)
        .then(res => {
            let data = res.data
            setArrStudent([...arrStudent, 
                {   
                    id: data.id,
                    name: data.name,
                    course: data.course,
                    score: data.score
                }
            ])
        })
    }

    const updateData = async () => {
        axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentId}`, input)
        .then(() => {
            let student = arrStudent.find(el=> el.id === currentId)
            student.name = input.name
            student.course = input.course
            student.score = input.score
            setArrStudent([...arrStudent])
        })
    }

    const deleteData = async (id) => {
        axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${id}`)
            .then(() => {
                let newArrStudent = arrStudent.filter(el=> {return el.id !== id})
                setArrStudent(newArrStudent)
        })
    }

    const functionEdit = async (id) => {
        axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${id}`)
        .then(res => {
            let data = res.data
            setInput({
                id: data.id,
                name: data.name,
                course: data.course,
                score: data.score
                } )
            setCurrentId(data.id)
          })
    }

    const clearForm =  () => {
        setInput({
            name: "",
            course: "",
            score: ""
        })
        setCurrentId(null)
    }

    const functions = {
        fetchData,
        createData,
        updateData,
        deleteData,
        functionEdit,
        clearForm
    }

    const states = {
        arrStudent, 
        setArrStudent,
        input,
        setInput,
        currentId,
        setCurrentId,
    }

    return (
        <StudentContext.Provider value={{
            states,
            functions
        }}>
          {props.children}
        </StudentContext.Provider>
    );
}