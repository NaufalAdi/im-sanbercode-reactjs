import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import Navbar from './Navbar';
import Tugas9 from '../Tugas-9/tugas9';
import Tugas10 from '../Tugas-10/tugas10'
import Tugas11 from '../Tugas-11/tugas11'
import Tugas12 from '../Tugas-12/tugas12'
import Tugas13 from '../Tugas-13/tugas13';
import Tugas14 from './tugas14';
import Tugas15 from '../Tugas-15/tugas15';
import './tugas14.css'
import { ThemeProvider } from './ThemeContext';

function Routes() {
    return (
        <Router>
            <ThemeProvider>
            <Navbar/>
            <Switch>
                <Route path="/" exact>
                    <Tugas9/>
                </Route>
                <Route path="/tugas10" >
                    <Tugas10/>
                </Route>
                <Route path="/tugas11" >
                    <Tugas11/>
                </Route>
                <Route path="/tugas11" >
                    <Tugas11/>
                </Route>
                <Route path="/tugas12"> 
                    <Tugas12/>
                </Route>
                <Route path="/tugas13">
                    <Tugas13/>
                </Route>
                <Route path="/tugas13">
                    <Tugas13/>
                </Route>
                <Route path="/tugas14">
                    <Tugas14/>
                </Route>
                <Route path="/tugas15">
                    <Tugas15/>
                </Route>
            </Switch>
            </ThemeProvider>
        </Router>
    )
}

export default Routes
