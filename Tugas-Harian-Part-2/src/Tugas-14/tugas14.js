import React, { useContext } from 'react'
import { StudentProvider } from './StudentContext'
import StudentForm from './StudentForm'
import StudentList from './StudentList'
import { ThemeContext } from './ThemeContext'

import {
    Switch,
    Route,
  } from "react-router-dom";

function Tugas14() {
    const{theme, setTheme} = useContext(ThemeContext)
    return (
        <StudentProvider>
            <Switch>
                <Route  exact path="/tugas14/create" component={StudentForm} />
                <Route  exact path="/tugas14/update" component={StudentForm} />
                <Route  exact path="/tugas14" >
                    <div class="button-center">
                    {theme === "light" ? (
                        <button id="theme-button" onClick={() => (setTheme("dark"))}>Change Navbar to Dark Theme</button>
                    ): (<button id="theme-button" onClick={() => (setTheme("light"))}>Change Navbar to Light Theme</button>
                    )}
                    </div>
                    <StudentList/>
                </Route>
            </Switch>
        </StudentProvider>
    )
}

export default Tugas14
